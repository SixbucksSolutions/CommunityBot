﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WazeBotDiscord.Wikisearch
{
    public class SearchItem
    {
        public string Title { get; set; }

        public string URL { get; set; }
    }
}
