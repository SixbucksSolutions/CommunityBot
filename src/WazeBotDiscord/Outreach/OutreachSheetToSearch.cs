﻿namespace WazeBotDiscord.Outreach
{
    public class OutreachSheetToSearch
    {
        public ulong ChannelId { get; set; }

        public ulong GuildId { get; set; }

        public string SheetId { get; set; }

        public string GId { get; set; }
    }
}
