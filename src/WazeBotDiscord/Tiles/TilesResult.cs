﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WazeBotDiscord.Tiles
{
    class TilesResult
    {
        public string NATileDate { get; set; }

        public string NAUpdatePerformed { get; set; }

        public string INTLTileDate { get; set; }

        public string INTLUpdatePerformed { get; set; }
    }
}
