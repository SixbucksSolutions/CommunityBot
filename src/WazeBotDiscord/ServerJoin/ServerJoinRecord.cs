﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WazeBotDiscord.ServerJoin
{
    public class ServerJoinRecord
    {
        public ulong GuildId { get; set; }

        public string JoinMessage { get; set; }
    }
}
