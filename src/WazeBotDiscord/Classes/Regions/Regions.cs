﻿namespace WazeBotDiscord.Classes.Servers
{
    public static class Servers
    {
        public const ulong National = 300471946494214146;
        public const ulong Northwest = 313435914540154890;
        public const ulong Southwest = 301113669696356352;
        public const ulong Plains = 313433524130545664;
        public const ulong SouthCentral = 313431377724964876;
        public const ulong GreatLakes = 299563059695976451;
        public const ulong SouthAtlantic = 300737538384199681;
        public const ulong Southeast = 313428729739083776;
        public const ulong NewEngland = 300482201198395417;
        public const ulong Northeast = 300481818619150336;
        public const ulong MidAtlantic = 299676784327393281;
        public const ulong CommunityTest = 360595895965843456;
        public const ulong WazeScripts = 347019166655709184;
        public const ulong GlobalMapraid = 347386780074377217;
        public const ulong VEOC = 356076662812573698;
        public const ulong ATR = 352258352417603587;
    }
}
