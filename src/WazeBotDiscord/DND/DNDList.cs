﻿using System;

namespace WazeBotDiscord.DND
{
    public class DNDListItem
    {
        public ulong UserId { get; set; }

        public DateTime EndTime { get; set; }
    }
}
