﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WazeBotDiscord.ServerLeave
{
    public class LeaveMessageChannel
    {
            public ulong ChannelId { get; set; }

            public ulong GuildId { get; set; }
    }
}
